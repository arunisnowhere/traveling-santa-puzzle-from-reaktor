function[tours, fval] = tsp_implement(dist, nStops)
    idxs = nchoosek(1:nStops,2);
    lendist = length(dist);
    tsp = optimproblem;
    trips = optimvar('trips',lendist,1,'Type','integer','LowerBound',0,'UpperBound',1);
        
    tsp.Objective = dist'*trips;
        
    constr2trips = optimconstr(nStops,1);
    for stops = 1:nStops
        whichIdxs = (idxs == stops);
        whichIdxs = any(whichIdxs,2); % start or end at stops
        constr2trips(stops) = sum(trips(whichIdxs)) == 2;
    end
    tsp.Constraints.constr2trips = constr2trips;
    
    opts = optimoptions('intlinprog','Display','off','Heuristics','round-diving',...
    'IPPreprocess','none');    
    [tspsol,fval,exitflag,output] = solve(tsp,'options',opts);

    tours = detectSubtours(tspsol.trips,idxs);
    numtours = length(tours); % number of subtours
    fprintf('# of subtours: %d\n',numtours);
        
    % Index of added constraints for subtours
    k = 1;
    while numtours > 1 % repeat until there is just one subtour
        % Add the subtour constraints
        for ii = 1:numtours
            subTourIdx = tours{ii}; % Extract the current subtour
            %         The next lines find all of the variables associated with the
            %         particular subtour, then add an inequality constraint to prohibit
            %         that subtour and all subtours that use those stops.
            variations = nchoosek(1:length(subTourIdx),2);
            a = false(length(idxs),1);
            for jj = 1:length(variations)
                whichVar = (sum(idxs==subTourIdx(variations(jj,1)),2)) & ...
                    (sum(idxs==subTourIdx(variations(jj,2)),2));
                a = a | whichVar;
            end
            tsp.Constraints.(sprintf('subtourconstr%i',k)) = sum(trips(a)) <= length(subTourIdx)-1;
            k = k + 1;
        end
        % Try to optimize again
        [tspsol,fval,exitflag,output] = solve(tsp,'options',opts);
                        
        % How many subtours this time?
        tours = detectSubtours(tspsol.trips,idxs);
        numtours = length(tours); % number of subtours
        fprintf('# of subtours: %d\n',numtours);
    end
end
