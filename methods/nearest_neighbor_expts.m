%% nearest-neighbor
% same as nearest-neighbor but experimenting with some variations.
% e.g., (1) for first n trips, choose the child getting the heaviest gifts
% rather than closest distance.
% (2) select min(distance/weight) instead of min(distance)
            
clear; close all; clc

%% Import data

load ../data/distances_weights_data.mat

% initialize
Santa_distance = (distance_matrix(1,2:end)); % Santa's distance to each child
Santa_distance_original = (distance_matrix(1,2:end)); % Santa's distance to each child
distance_matrix(:,1) = 0; % remove the mirror image of Santa_distance
children_distance = (distance_matrix(2:end,2:end)); % children-children distance
children_distance(children_distance==0)=NaN; % make the diagonal NaN
children_distance_original = children_distance;
Santa_trips = {}; % catch all Santa's trips
total_distance_temp = 0; % catch total distance traveled by Santa

weights_temp = weights_all;

count = 0;

% while there are any child visits left: NaN => child is visited
while sum(isnan(Santa_distance)) < length(Santa_distance)
    sum(isnan(Santa_distance))
    count = count + 1;
    weight_trip = 0; % initialize
    % find first child
%     if mod(count,2)
    if count > 150
        [first_child_distance, first_child_index] = min(Santa_distance);
    else
        %         [first_child_distance, first_child_index] = min((Santa_distance.^5)./(weights_temp'.^2)); 
%         [first_child_distance, first_child_index] = max(weights_temp'./(Santa_distance));
        [first_child_distance, first_child_index] = max(weights_temp);
    end
    % add weight of first child
    weight_trip = weight_trip + weights_all(first_child_index);
    % add distance to first child
    total_distance_temp = total_distance_temp + first_child_distance;
    % which child is Santa visiting right now?
    child_now_index = first_child_index;
    % remove child_n from the distance to Santa
    Santa_distance(child_now_index) = NaN;
    weights_temp(child_now_index) = NaN;
    % build schedule for the trip
    child_trip = child_now_index;
        
    while true
        % who is nearest child to current child?
        [next_child_distance, next_child_index] = min(children_distance(child_now_index,:));
%         [next_child_distance, next_child_index] = min(((children_distance(child_now_index,:)).^10)./(weights_temp'.^1));
%         [next_child_distance, next_child_index] = max((weights_temp'.^3)./(children_distance(child_now_index,:)).^1);
%         children_distance_temp = children_distance;        
%         while Santa_distance(next_child_index) < next_child_distance
%             children_distance_temp(next_child_index,:) = NaN;
%             children_distance_temp(:,next_child_index) = NaN;
%             [next_child_distance, next_child_index] = min(children_distance_temp(child_now_index,:));            
%         end
%         
        if (weight_trip + weights_all(next_child_index) <= 10000000) &&... 
                (sum(isnan(Santa_distance)) < length(Santa_distance)) % 10000 kg            
            % add weight
            weight_trip = weight_trip + weights_all(next_child_index);
            % add distance
            total_distance_temp = total_distance_temp + next_child_distance;           
            
            % remove child_n from the child-child distances          
            children_distance(child_now_index,:) = NaN;
            children_distance(:,child_now_index) = NaN;
            
            % child_n+1 is now child_n
            child_now_index = next_child_index;
            % remove child_n from the distance to Santa
            Santa_distance(child_now_index) = NaN;
            weights_temp(child_now_index) = NaN;
            % build schedule for the trip
            child_trip = [child_trip child_now_index];           
        else
            break
        end
    end    
    children_distance(child_now_index,:) = NaN;
    children_distance(:,child_now_index) = NaN;    
    Santa_trips{end + 1} = child_trip + 1; % +1 needed because the puzzle indexes from 2:100001!
    Santa_new_distance = (distance_matrix(1,2:end));
    total_distance_temp = total_distance_temp + Santa_new_distance(child_now_index);
    
end
total_distance_temp

total_distance_actual = 0;
for i = 1:length(Santa_trips)
    new_child_trip = Santa_trips{1,i} - 1;
    distance_new = 0;
for j = 1:length(new_child_trip)-1    
    distance_new = distance_new + children_distance_original(new_child_trip(1,j), new_child_trip(1,j+1));
end
distance_new = distance_new + Santa_distance_original(new_child_trip(1)) + Santa_distance_original(new_child_trip(end));
total_distance_actual = total_distance_actual + distance_new;
end
total_distance_actual


% save ('solution.mat')

fid = fopen('solution.csv','wt');
if fid>0
    for i=1:length(Santa_trips)
        for j = 1:length(Santa_trips{:,i})
            if j == length(Santa_trips{:,i})
                fprintf(fid,'%d',Santa_trips{:,i}(1,j));
            else
                fprintf(fid,'%d; ',Santa_trips{:,i}(1,j));
            end
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
end