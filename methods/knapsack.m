%% knapsack
% basic idea:
% (1) repeat till all children are supplied:
    % (1) Santa chooses closest remaining child 
    % (2) Santa gives gift to child
    % (3) use knapsack milp to find optimal children to give gifts       

% in practice:
% (1) repeat till all children are supplied:
    % (1) Santa chooses closest available child 
    % (2) Santa gives gift to child
    % (3) calculate weight delivered, distance traveled
    % (4) remove child from list of Santa's potential first visits
    % (5) use knapsack milp to find optimal children to give gifts 
    % (6) calculate total weight and total distance
    % (7) remove all selected children from list
    
% Note: Incorrect implementation, does not work correctly
     
clear; close all; clc
tic

%% Import data

load ../data/distances_weights_data.mat

% initialize
Santa_distance = (distance_matrix(1,2:end)); % Santa's distance to each child
distance_matrix(:,1) = 0; % remove the mirror image of Santa_distance
children_distance = (distance_matrix(2:end,2:end)); % children-children distance
children_distance(children_distance==0)=NaN; % make the diagonal NaN
Santa_trips = {}; % catch all Santa's trips
total_distance = 0; % catch total distance traveled by Santa

% while there are any child visits left: NaN => child is visited
% while sum(isnan(Santa_distance)) < length(Santa_distance)
    childen_remaining = length(Santa_distance) - sum(isnan(Santa_distance)) % just for the loop status
    
    % Santa chooses the first child based on minimum distance
    [first_child_distance, first_child_index] = min(Santa_distance);
    % find nearest n children to this child   
    nChildren = min(4, childen_remaining); % n = some number or remaining children
            
    % sort distances and indexes in increasing order
    [children_distance_sorted, children_index_sorted] = sort(children_distance(first_child_index,:));
    % select the first nChildren
    selected_children_indexes = children_index_sorted(1:nChildren);
    % note: include first selected child as well
    selected_children_all_indexes = [first_child_index selected_children_indexes];
    
    % make a distance matrix for the selected children
    all_distances_selected_children = zeros(nChildren+1); % +1 is to include first_child_index as well
    for i = 1:nChildren+1
        for j = 1:nChildren+1
            all_distances_selected_children(i,j) = ...
                children_distance(selected_children_all_indexes(i),selected_children_all_indexes(j));
        end
    end
    
    % unroll the distances into a vector.
    % note that d11, d22, d33, ..., djj are eliminated
    % Let nChildren = n. note that nChildren does not include first child
    % d(1:n) = x_12:x_1n
    % d(n+1):d(n*2) = x_21:x_2n
    % d(n*2+1):d(n*3) = x_31:x_3n
    % and so on...
    % d((n*n)+1:(n+1)*n) = x_(n+1)1:x_(n+1)n    
    a = all_distances_selected_children;
    a(isnan(a)) = 0;
    distances_selected_children = (nonzeros(a'))'; % 
    
    return
    
    % MBLP
    % objective
    f = distances_selected_children; % 1 x n(n+1)
    
    % eq constraint 1: child1 is already selected
    Aeq = [ones(1, nChildren) zeros(1,(nChildren)*(nChildren))]; % 1 x n(n+1)
    beq = 1; % 1
    
    % ineq constraint 1: only 1 child can go to 1 child
    a_ineq1 = []; 
    for i = 1:nChildren+1
        a_ineq1 = [a_ineq1 eye(nChildren)];
    end
    % n x n(n+1)
    b_ineq1 = ones(nChildren+1,1); % n x 1
        
    % ineq constraint 2: except for first child, others can only have one
    % value horizontally
    n = nChildren; % for simplicity
    a_ineq2 = zeros(n+1,n*(n+1));
    for i = 2:n+1
        a_ineq1(i,(i-1)*n+1:i*n) = ones(1,n);        
    end
    b_ineq2 = [0; ones(nChildren,1)]; % (n+1) x 1
    
    % ineq constraint 3: weight constraint
    % since first child is already selected, set its weight as 0
    weights_vector = [0 weights_all(selected_children_indexes)'];
    weights = repmat(weights_vector', 1, nChildren);
    % constraint 3a: maximum weight constraint
    a_ineq3a = reshape(weights',1,nChildren*(nChildren+1));
    b_ineq3a = 10000000;
    % constraint 3b: minimum weight constraint
    a_ineq3b = -a_ineq3a;
    b_ineq3b = -10000000+10000;    
    
            
    Aineq = [a_ineq1; a_ineq2; a_ineq3a; a_ineq3b];
    bineq = [b_ineq1; b_ineq2; b_ineq3a; b_ineq3b];
    lb = zeros(1,nChildren*(nChildren+1));
    ub = ones(1,nChildren*(nChildren+1));

    intcon = 1:1:nChildren*(nChildren+1);
    
%     size(f)
%     size(Aineq)
%     size(bineq)
%     size(Aeq)
%     size(beq)
%     size(lb)
%     size(ub)
        
    intlinprogOptions = optimoptions('intlinprog', 'Display', 'off');
    [x, fval] = intlinprog(f, intcon, Aineq, bineq, Aeq, beq, lb, ub,[] , intlinprogOptions);
    
    trip_indexes_temp = find(x==1);
    trip_indexes = [first_child_index selected_children_all_indexes(nonzeros(floor(trip_indexes_temp/nChildren)))];
       
    % d(1:n) = x_12:x_1n
    % d(n+1):d(n*2) = x_21:x_2n
    % d(n*2+1):d(n*3) = x_31:x_3n
    % and so on...
    % d((n*n)+1:(n+1)*n) = x_(n+1)1:x_(n+1)n
        
    % total weight of the selected trip
    trip_weight = sum(weights_all(trip_indexes));
%     if trip_weight > 10000000, disp('error!'), return, end
    % total distance covered in the selected trip
    trip_distance = fval + Santa_distance(first_child_index) + Santa_distance(trip_indexes(end));
    % add trip_distance to total distance
    total_distance = total_distance + fval;  
    % remove all_selected_children from the distance to Santa
    Santa_distance(trip_indexes) = NaN;
    % remove child_n from the child-child distances          
    children_distance(trip_indexes,:) = NaN;
    children_distance(:,trip_indexes) = NaN;
    % collect the trip indexes
    Santa_trips{end + 1} = trip_indexes + 1; % +1 needed because the puzzle indexes from 2:100001!
% end
    
total_distance
toc
return


save ('solution.mat')

fid = fopen('solution.csv','wt');
if fid>0
    for i=1:length(Santa_trips)
        for j = 1:length(Santa_trips{:,i})
            if j == length(Santa_trips{:,i})
                fprintf(fid,'%d',Santa_trips{:,i}(1,j));
            else
                fprintf(fid,'%d; ',Santa_trips{:,i}(1,j));
            end
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
end

Santa_trips_new = {}; % catch all Santa's trips
for i = 1:length(Santa_trips)
    trip_index = unique(Santa_trips{1,i}, 'stable');
    Santa_trips_new{end + 1} = trip_index;    
end


fid = fopen('solution.csv','wt');
if fid>0
    for i=1:length(Santa_trips_new)
        for j = 1:length(Santa_trips_new{:,i})
            if j == length(Santa_trips_new{:,i})
                fprintf(fid,'%d',Santa_trips_new{:,i}(1,j));
            else
                fprintf(fid,'%d; ',Santa_trips_new{:,i}(1,j));
            end
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
end
