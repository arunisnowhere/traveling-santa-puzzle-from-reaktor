%% nearest-neighbor-tsp
% basically, same as nearest-neighbor, but with classical tsp added for efficiency

% basic idea:
% (1) repeat till all children are supplied:
    % (1) Santa chooses closest remaining child 
    % (2) Santa gives gift to child
    % (3) repeat till maximum weight is reached:
        % (1) find the closest remaining child from previous child(n), child(n+1)
        % (2) go to child(n+1)
    % (4) apply classical tsp to efficiently connect the chosen children

% in practice:
% (1) repeat till all children are supplied:
    % (1) Santa chooses closest available child 
    % (2) Santa gives gift to child
    % (3) calculate weight delivered, distance traveled
    % (4) remove child from list of Santa's potential first visits
    % (3) repeat for all available children:
        % (1) find the closest remaining child from previous child(n), child(n+1)
        % (2) will the new weight be greater than maximum allowed weight?
        % If yes,
            % (a) Santa returns home and begins new delivery.
        % If no,
            % (a) Santa goes to child(n+1)      
            % (b) update weight delivered, distance
            % (c) remove child from list of Santa's potential first visits
            % (Santa-child)
            % (d) remove child from list of all possible Santa's visits
            % (child_child)
    % (4) apply classical tsp
    
clear; close all; clc
tic

%% Import data
load ../data/distances_weights_data.mat
% add path to dependencies
addpath dependencies/

% initialize
Santa_distance = (distance_matrix(1,2:end)); % Santa's distance to each child
Santa_distance_original = Santa_distance;
distance_matrix(:,1) = 0; % remove the mirror image of Santa_distance
children_distance = (distance_matrix(2:end,2:end)); % children-children distance
children_distance(children_distance==0)=NaN; % make the diagonal NaN
children_distance_original = children_distance;

Santa_trips = {}; % catch all Santa's trips
total_distance_nn = 0; % catch total distance traveled by Santa

count = 0;

% while there are any child visits left: NaN => child is visited
while sum(isnan(Santa_distance)) < length(Santa_distance)
    sum(isnan(Santa_distance))
    count = count + 1;
    weight_trip = 0; % initialize    
    
    [first_child_distance, first_child_index] = min(Santa_distance);

    % add weight of first child
    weight_trip = weight_trip + weights_all(first_child_index);
    % add distance to first child
    total_distance_nn = total_distance_nn + first_child_distance;
    % which child is Santa visiting right now?
    child_now_index = first_child_index;
    % remove child_n from the distance to Santa
    Santa_distance(child_now_index) = NaN;
    
    % build schedule for the trip
    child_trip = child_now_index;
        
    while true
        % who is nearest child to current child?
        [next_child_distance, next_child_index] = min(children_distance(child_now_index,:));
        
        if (weight_trip + weights_all(next_child_index) <= 10000000) &&... 
                (sum(isnan(Santa_distance)) < length(Santa_distance)) % 10000 kg            
            % add weight
            weight_trip = weight_trip + weights_all(next_child_index);
            % add distance
            total_distance_nn = total_distance_nn + next_child_distance;    
            
            % remove child_n from the child-child distances          
            children_distance(child_now_index,:) = NaN;
            children_distance(:,child_now_index) = NaN;
            
            % child_n+1 is now child_n
            child_now_index = next_child_index;
            % remove child_n from the distance to Santa
            Santa_distance(child_now_index) = NaN;
            
            % build schedule for the trip
            child_trip = [child_trip child_now_index];           
        else
            break
        end
    end
    
    children_distance(child_now_index,:) = NaN;
    children_distance(:,child_now_index) = NaN;
    Santa_trips{end + 1} = child_trip+1;     
    % +1 needed because the puzzle indexes from 2:100001!
    
    total_distance_nn = total_distance_nn + Santa_distance_original(child_now_index);
    
end
total_distance_nn = % 8.1360e+09

%% tsp part
% now that you have a list of all children, solve a tsp for this list, with Santa as well
Santa_new_trips = {}; % catch all Santa's new trips
count = 0;
total_distance_nn_check = 0;
total_distance_nn_tsp = 0;
for i = 1:length(Santa_trips)
    i % just to check status
    child_trip = Santa_trips{1,i}-1;
    
    if length(child_trip) > 1 % if there is more than one child in the trip
        nStops = length(child_trip)+1; % + Santa    

        % distance
        selected_indexes_distance = zeros(length(child_trip));
        for j = 1:length(child_trip)
            selected_indexes_distance(j,:) = children_distance_original(child_trip(j),child_trip);
        end    

        a = selected_indexes_distance;

        a(isnan(a)) = 0;

        new_Santa_distances = Santa_distance_original(child_trip);
        a = [new_Santa_distances' a];    
        a = [0 new_Santa_distances; a];
        a = triu(a);
        distances_selected_children_temp = (nonzeros(a'))'; %
        dist = distances_selected_children_temp';
    
        % call tsp function
        [tours, fval] = tsp_implement(dist, nStops);

        % determine new indexes
        new_indexes = tours{1,1};
        new_indexes(new_indexes==1)=[];
        new_indexes = (new_indexes - 1);
        new_child_trip = child_trip(new_indexes);    

    else
        new_child_trip = child_trip;
    end
    
    distance_old = 0;
    for j = 1:length(child_trip)-1
        distance_old = distance_old + children_distance_original(child_trip(1,j), child_trip(1,j+1));
    end
    distance_old = distance_old + Santa_distance_original(child_trip(1)) + Santa_distance_original(child_trip(end));
    total_distance_nn_check = total_distance_nn_check + distance_old;
    
    distance_new = 0;
    for j = 1:length(new_child_trip)-1
        distance_new = distance_new + children_distance_original(new_child_trip(1,j), new_child_trip(1,j+1));
    end
    distance_new = distance_new + Santa_distance_original(new_child_trip(1)) + Santa_distance_original(new_child_trip(end));
    total_distance_nn_tsp = total_distance_nn_tsp + distance_new;
    
    Santa_new_trips{end + 1} = new_child_trip + 1; % +1 needed because the puzzle indexes from 2:100001!
    
end
total_distance_nn_check % 8.1360e+09
total_distance_nn_tsp % 8.0667e+09

total_distance_nn_tsp_check = 0;
for i = 1:length(Santa_new_trips)
    new_child_trip = Santa_new_trips{1,i} - 1;
    distance_new = 0;
for j = 1:length(new_child_trip)-1    
    distance_new = distance_new + children_distance_original(new_child_trip(1,j), new_child_trip(1,j+1));
end
distance_new = distance_new + Santa_distance_original(new_child_trip(1)) + Santa_distance_original(new_child_trip(end));
total_distance_nn_tsp_check = total_distance_nn_tsp_check + distance_new;
end
total_distance_nn_tsp_check % 8.0667e+09

%% prepare solution
% save ('solution.mat')
Santa_new_trips = Santa_trips;
fid = fopen('solution.csv','wt');
if fid>0
    for i=1:length(Santa_new_trips)
        for j = 1:length(Santa_new_trips{:,i})
            if j == length(Santa_new_trips{:,i})
                fprintf(fid,'%d',Santa_new_trips{:,i}(1,j));
            else
                fprintf(fid,'%d; ',Santa_new_trips{:,i}(1,j));
            end
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
end

toc