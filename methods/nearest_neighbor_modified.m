%% nearest-neighbor-modified
% same as nearest-neighbor but with an additional modification:
% when the closest child's weight makes the total weight more than max
% allowed, check if there is any child whose weight can still be
% accommodated? if there are such children, go to the closest one, if that
% child is closer than simply returning to Santa

% basic idea:
% (1) repeat till all children are supplied:
% (1) Santa chooses closest remaining child
% (2) Santa gives gift to child
% (3) repeat till maximum weight is reached:
% (1) find the closest remaining child from previous child(n), child(n+1)
% (2) go to child(n+1)
% check if any child can still be gifted? give gift to that child with
% minimum distance, if minimum distance < distance to Santa

% in practice:
% (1) repeat till all children are supplied:
% (1) Santa chooses closest available child
% (2) Santa gives gift to child
% (3) calculate weight delivered, distance traveled
% (4) remove child from list of Santa's potential first visits
% (3) repeat for all available children:
% (1) find the closest remaining child from previous child(n), child(n+1)
% (2) will the new weight be greater than maximum allowed weight?
% If yes,
% (a) Santa returns home and begins new delivery.
% If no,
% (a) Santa goes to child(n+1)
% (b) update weight delivered, distance
% (c) remove child from list of Santa's potential first visits
% (Santa-child)
% (d) remove child from list of all possible Santa's visits
% (child_child)

clear; close all; clc

%% Import data

load ../data/distances_weights_data.mat

% initialize
Santa_distance = (distance_matrix(1,2:end)); % Santa's distance to each child
Santa_distance_original = Santa_distance;
distance_matrix(:,1) = 0; % remove the mirror image of Santa_distance
children_distance = (distance_matrix(2:end,2:end)); % children-children distance
children_distance(children_distance==0)=NaN; % make the diagonal NaN
children_distance_original = children_distance;
Santa_trips = {}; % catch all Santa's trips
total_distance = 0; % catch total distance traveled by Santa

% while there are any child visits left: NaN => child is visited
while sum(isnan(Santa_distance)) < length(Santa_distance)
    sum(isnan(Santa_distance))
    
    weight_trip = 0; % initialize
    % find first child
    [first_child_distance, first_child_index] = min(Santa_distance);
    % add weight of first child
    weight_trip = weight_trip + weights_all(first_child_index);
    % add distance to first child
    total_distance = total_distance + first_child_distance;
    % which child is Santa visiting right now?
    child_now_index = first_child_index;
    % remove child_n from the distance to Santa
    Santa_distance(child_now_index) = NaN;
    % build schedule for the trip
    child_trip = child_now_index;
    
    while true
        % who is nearest child to current child?
        [next_child_distance, next_child_index] = min(children_distance(child_now_index,:));
        
        if (weight_trip + weights_all(next_child_index) <= 10000000) &&...
                (sum(isnan(Santa_distance)) < length(Santa_distance)) % 10000 kg
            % add weight
            weight_trip = weight_trip + weights_all(next_child_index);
            % add distance
            total_distance = total_distance + next_child_distance;
            
            % remove child_n from the child-child distances
            children_distance(child_now_index,:) = NaN;
            children_distance(:,child_now_index) = NaN;
            
            % child_n+1 is now child_n
            child_now_index = next_child_index;
            % remove child_n from the distance to Santa
            Santa_distance(child_now_index) = NaN;
            % build schedule for the trip
            child_trip = [child_trip child_now_index];
            
        else
            % is there any weight that can still fit?
            weight_added_remaining_children = zeros(1,length(Santa_distance));
            for i = 1:length(Santa_distance)
                if isnan(Santa_distance(i))
                    weight_added_remaining_children(i) = 10000000+1;
                else
                    weight_added_remaining_children(i) = weight_trip + weights_all(i);
                end
            end
            
            % which children can still fit the weight?
            children_adding_enough_weight = zeros(1,length(Santa_distance));
            for i = 1:length(Santa_distance)
                if weight_added_remaining_children(i) <= 10000000
                    children_adding_enough_weight(i) = i;
                end
            end
            
            if sum(children_adding_enough_weight>0) > 0 % if there are enough such children
                % find distance to closest such child
                [next_child_distance, ~] = ...
                    min(children_distance(child_now_index,find(children_adding_enough_weight>0)));
                % find the closest such child
                next_child_index = find(children_distance_original(child_now_index,:)==next_child_distance);
                
                % if Santa is closer than child, return to Santa
                if Santa_distance_original(child_now_index) < next_child_distance
                    break;
                end
                
                % add weight
                weight_trip = weight_trip + weights_all(next_child_index);
                % add distance
                total_distance = total_distance + next_child_distance;
                
                % remove child_n from the child-child distances
                children_distance(child_now_index,:) = NaN;
                children_distance(:,child_now_index) = NaN;
                
                % child_n+1 is now child_n
                child_now_index = next_child_index;
                % remove child_n from the distance to Santa
                Santa_distance(child_now_index) = NaN;
                % build schedule for the trip
                child_trip = [child_trip child_now_index];
            else
                break
            end
        end
    end
    children_distance(child_now_index,:) = NaN;
    children_distance(:,child_now_index) = NaN;
    Santa_trips{end + 1} = child_trip + 1; % +1 needed because the puzzle indexes from 2:100001!
    Santa_new_distance = (distance_matrix(1,2:end));
    total_distance = total_distance + Santa_new_distance(child_now_index);
    
end
total_distance % 9.3668e+09

fid = fopen('solution.csv','wt');
if fid>0
    for i=1:length(Santa_trips)
        for j = 1:length(Santa_trips{:,i})
            if j == length(Santa_trips{:,i})
                fprintf(fid,'%d',Santa_trips{:,i}(1,j));
            else
                fprintf(fid,'%d; ',Santa_trips{:,i}(1,j));
            end
        end
        fprintf(fid,'\n');
    end
    fclose(fid);
end