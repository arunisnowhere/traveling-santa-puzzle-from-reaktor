Traveling Santa Puzzle
============================
Contains solutions to Traveling Santa Puzzle, set by Reaktor.

Written in Matlab

## data folder
contains the original data and data manipulation files and databases

nicelist.txt: given csv file

nicelist.mat: converted to *.mat

find_distance_between_two_points_Earth.mat: functiont to calculate distance between two points on the surface of the earth (uses Spherical Law of Cosines)

create_distances_weights_data.m: script to converts all given data to a weight vector and a distance matrix.

distances_weights_data.mat: mat file with all given data as a weight vector and a distance matrix.

## methods folder
contains the methods used to solve the puzzle along with their solutions

knapsack.m: (incomplete) attempted solution by adapting knapsack problem (assume that value is distance, and obj = min(distance) rather than max(value)).

nearest_neighbor.m and solution_nearest_neighbor.csv: uses a nearest neighbor heuristic brute-forcish technique. solution: 8.1360e+09

nearest_neighbor_expts: same as nearest-neighbor but with some experiments with some variations

nearest_neighbor_modified and solution_nearest_neighbor_modified.csv: same as nearest-neighbor but with an additional modification to also consider remaining weights. solution: 9.3668e+09

nearest_neighbor_tsp and solution_nearest_neighbor_tsp.csv: nearest neighbor with a tsp layer. solution: 8.0667e+09

nearest_neighbor_tsp_weights and solution_nearest_neighbor_tsp_weights.csv: nearest neighbor with a tsp layer and considering maximum weights for first n trips. solution: 7.9679e+09

## dependencies sub-folder (of methods)
containts any dependencies required by the methods

tsp_implement.m: implements tsp using MILP, taken from Matlab Documentation

detectSubtours.m: a dependency of tsp_implement.m, to eliminate subtours

tsp_example.m: an example to illustrate tsp_implement.m

## Contributors
All codes written by Arun Narayanan

[between December 28, 2018 and December 31, 2018;

Cleaned up on January 1, 2019;

Uploaded to Gitlab after another cleanup on January 10, 2019]
