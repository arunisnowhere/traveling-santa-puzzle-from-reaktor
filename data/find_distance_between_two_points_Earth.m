function[distance]=find_distance_between_two_points_Earth(latitude1, longitude1, latitude2, longitude2, radiusEarth)
% uses Spherical Law of Cosines to calculate distance between two points on
% the surface of the earth.
% Ref.: https://maftahur.wordpress.com/2010/03/30/finding-distance-between-two-points-given-in-latitude-and-longitude-points/
% Ref.: https://en.wikipedia.org/wiki/Great-circle_distance

deltaLongitude = longitude2 - longitude1;

% Using Spherical Law of Cosine
distance = (acos(sin(latitude1)*sin(latitude2)+cos(latitude1)*cos(latitude2)*cos(deltaLongitude)))*radiusEarth;
% The Spherical Low of Cosine gives more computational precision than Haversine formula and finds distance as small as 1m.

end

% % Using Haversine formula
% deltaLatitude = latitude2 - latitude1;
% deltaLongitude = longitude2 - longitude1;
% a = (sin(deltaLatitude/2))^2 + cos(latitude1)*cos(latitude2)*(sin(deltaLongitude/2)^2);
% C = 2*atan2(sqrt(a), sqrt(1-a));
% distance1 = radiusEarth*C;

% for more accuracy (mm accuracy), use Vincenty solutions of geodesics on
% the ellipsoid (http://www.movable-type.co.uk/scripts/latlong-vincenty.html)