clear; close all; clc
tic

% converts all given data to a weight vector and a distance matrix.
% in distance matrix, rows and columns 1:100001 = Santa:10000th child
% so, for example, (1,2) => distance from Santa to child 1
% (10,20) => distance from child 9 to child 19
% (1000,240) => distance from child 999 to child 239
% i.e., (i,j) => distance from child i-1 to child j-1, for all i,j =
% 1:100001 and i,j = 1 => Santa
% Note: diagonals are 0


%% data
load nicelist.mat

% Santa starts at Korvatunturi, Finland (68.073611N 29.315278E)
SantaData = zeros(length(data)+1, 4);
SantaData(1,:) = [0 68.073611 29.315278 0];
SantaData(2:end,:) = data;

radiusEarth = 6378000; % m

%% create distance matrix 
% distance matrix = Santa-child and child-child distances
% Santa = row 1, column 1; child 1 to 10000 are rows 2 to 100001, columns 2
% to 100001

% initialize
distance_matrix = zeros(length(SantaData));

for i = 1:length(SantaData)
    i % just to count on the display
    for j = i+1:length(SantaData)
        latitude1 = (SantaData(i,2)/180 )*pi; % change to radians
        longitude1 = (SantaData(i,3)/180)*pi; % change to radians
        latitude2 = (SantaData(j,2)/180 )*pi; % change to radians
        longitude2 = (SantaData(j,3)/180)*pi; % change to radians

        distance_matrix(i,j) = find_distance_between_two_points_Earth(latitude1, longitude1, ...
            latitude2, longitude2, radiusEarth);
    end
end

% flip half of matrix over the diagonal to make a symmetric matrix
distance_matrix = (distance_matrix+distance_matrix') - eye(size(distance_matrix,1)).*diag(distance_matrix);
weights_all = data(:,4);

% save the data
% note: distance in metres, weight in grams
save('distances_weights_data.mat', 'distance_matrix', 'weights_all')

toc